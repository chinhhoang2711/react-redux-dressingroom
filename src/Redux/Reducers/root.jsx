import { combineReducers } from "redux";
import ProductReducer from "./product";
import CategoryReducer from "./category";
import choosenCategoryReducer from "./choosenCategory";
import modelReducer from "./model";

const RootReducer = combineReducers({
  //Danh sách state lưu trữ trên Store
  products: ProductReducer,
  categories: CategoryReducer,
  choosenCategory: choosenCategoryReducer,
  model: modelReducer,
});

export default RootReducer;
