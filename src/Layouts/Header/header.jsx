import React, { Component } from "react";

class HeaderComponent extends Component {
  render() {
    return (
      <div>
        <div classname="row">
          <div classname="card hovercard">
            <div classname="card-background" />
            <div classname="useravatar">
              <img alt="cybersoft.edu.vn" src="images/cybersoft.png" />
            </div>
            <div classname="card-info">
              <span classname="card-title">
                CyberSoft.edu.vn - Đào tạo chuyên gia lập trình - Dự án thử đồ
                trực tuyến - Virtual Dressing Room
              </span>
            </div>
          </div>
        </div>
        <hr classname="style13" />
      </div>
    );
  }
}

export default HeaderComponent;
