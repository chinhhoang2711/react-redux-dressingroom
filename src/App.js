import React from "react";
import "./App.css";
import HomeScreen from "./screens/Home/home";

function App() {
  return (
    <div>
      <HomeScreen />
    </div>
  );
}

export default App;
